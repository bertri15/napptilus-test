Napptilus test web app developed by Bertran Guasch Parera
The final project can be found at >> http://project-bertri.rf.gd/test <<

This app has been developed with React. To create the project the create-react-app library
has been used in order to ease the process (https://github.com/facebook/create-react-app).

This app has 2 parts. The source code can be found on the "src" folder being the main app
on the "App.js" file, and the main style on "App.css". I recommend looking at these files
only.
The final build which is the one present on my website can be found at the "build" folder.
I do not recommend looking at these files since they are optimized. These files can be
uploaded to a server but they have been compiled to work on >> root/test/ << folder. So if 
you need to upload on another folder you should build again the program chaning the 
"homepage" variable on the file "package.json".

App.js summary:
I've tried to create a component "App" that acts as the first hierarchy and does all the
operations and stores all the data.
App loads the "List" of Oompas and the "SearchBar" if it's on the view=0, and if a Oompa
has been selected, then the view=1 loads with the "Specific" component. The filtering has
been done on the "List" component with the filters passed by the "App" component.

Comments:
-First of all I'm aware of some bugs that I haven't fixed
--I'm not very familiarized with browser history manipulation so I do believe the use of
endpoint could be improved
--I've used localstorage to store previous data with a Date variable to know if it's still
relevant to the date. The problem is that I update the Date every time I recieve new data,
but this means that older data gets a new date. I should have used a date for every
element instead of all the list of Oompas.
--Style related. I haven't managed to vertically center the text on the header.