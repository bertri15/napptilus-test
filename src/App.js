import React, { Component } from 'react';
import './App.css';

class Header extends Component {
	render(){
		return (
			<div className="HeaderContainer" onClick={this.props.mainView}>
				<img src="https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/logo-umpa-loompa.png" />
				<span>Oompa Loompa's Crew</span>
			</div>
		)
	}
}

class SearchBar extends Component {
	constructor(props) {
    super(props);
    this.state = {
    }
  }
	render() {
		return(
			<div className="SearchBar">
				<input onChange={this.props.inputHandler} placeholder="Search" /><img src="https://s3.eu-central-1.amazonaws.com/napptilus/level-test/imgs/ic_search.png"/>
			</div>
		);
	}
}

class ListContainer extends Component {
	constructor(props) {
    super(props);
    this.state = {
			
    }
  }
	render() {
		var FinalList = this.props.list.slice(0);
		var filterCount = 0;
		while (filterCount < this.props.filters.length){
			var CurrentList = FinalList.slice(0);
			var listCount = 0;
			FinalList=[];
			while (listCount < CurrentList.length){
				if (CurrentList[listCount].first_name.toUpperCase().includes(this.props.filters[filterCount].toUpperCase()) ||
				CurrentList[listCount].last_name.toUpperCase().includes(this.props.filters[filterCount].toUpperCase()) ||
				CurrentList[listCount].profession.toUpperCase().includes(this.props.filters[filterCount].toUpperCase())){
					FinalList.push(CurrentList[listCount])
				}
				listCount++;
			}
			filterCount++;
		}
		const render = FinalList.map((element) => 
			<div className="ListElement" key={element.id} onClick={() => this.props.expandHandler(element.id)}> 
				<img src={element.image} />
				<p><b>{element.first_name} {element.last_name}</b></p>
				<div>
					<p>{element.gender === "M" ? "Male" : "Female"}</p>
					<p>{element.profession}</p>
				</div>
			</div>
		);
		return(
			<div>
				{render}
			</div>
		);
	}
}

class List extends Component {
	constructor(props) {
    super(props);
    this.state = {
    }
  }
	render() {
		return(
			<div className="Content">
				<div className="Separator">
					<SearchBar inputHandler={this.props.inputHandler}/>
				</div>
				<div className="Titles Separator">
					<h1> Find your Oompa Loompa </h1>
					<h3> There are more than 100k </h3>
				</div>
				<ListContainer expandHandler={this.props.expandHandler} filters={this.props.filters} list={this.props.list} />
			</div>
		);
	}
}

class Specific extends Component {
	constructor(props) {
    super(props);
    this.state = {
    }
  }
	render() {
		return(
			<div className="Content">
				<div className="Half">
					<img src={this.props.specific.image} className="" />
				</div>
				<div className="Half">
					<h3>{this.props.specific.first_name} {this.props.specific.first_name}</h3>
					<div>
						<p>{this.props.specific.gender === "M" ? "Male" : "Female"}</p>
						<p><i>{this.props.specific.profession}</i></p>
					</div>
					<p dangerouslySetInnerHTML={{__html: this.props.specific.description}} />
				</div>
			</div>
		);
	}
}

class App extends Component {
	constructor(props) {
    super(props);
    this.state = {
			loading: false,
      view: 0,
			page: 0,
			list: [],
			specific: {},
			filters: []
    };
		this.HandleScroll = this.HandleScroll.bind(this);
		this.LoadMore = this.LoadMore.bind(this);
		this.inputHandler = this.inputHandler.bind(this);
		this.expandHandler = this.expandHandler.bind(this);
		this.mainView = this.mainView.bind(this);
  }
	LoadMore(){
		this.setState({
			loading: true
		});
		var notAvailable = true;
		var result = localStorage.getItem('Data');   //Manage stored/new data
		if (result){
			result = JSON.parse(result);
			if(Math.ceil((new Date() - new Date(result.date)) / 8.64e7) < 2){ //if the data is still relevant
				var count = 0;
				while (count < result.info.length){
					if (result.info[count].page === (this.state.page+1)){
						var newList = this.state.list.slice(0);
						newList = newList.concat(result.info[count].list);
						this.setState({
							loading: false,
							page: this.state.page+1,
							list: newList
						});
						notAvailable = false;
						count = result.info.length;
					}
					count++;
				}
			}
			else {
				localStorage.removeItem('Data');
			}
		}
		if (notAvailable){
			var NewPage = () => {
				fetch('https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas?page='+(this.state.page+1))
					.then(response => response.json())
					.then(data => {
						var newList = this.state.list.slice(0);
						newList = newList.concat(data.results);
						this.setState({
							loading: false,
							page: this.state.page+1,
							list: newList
						});
						//store new data
						var newDate = new Date();
						var newInfo = [];
						if (result){
							newInfo = result.info;
						}
						newInfo.push({page: this.state.page, list: data.results});
						var newObject = {date: newDate, info: newInfo};
						localStorage.setItem('Data', JSON.stringify(newObject));
				})
				.catch(err => console.error(this.props.url, err.toString()))
			}
			NewPage();
		}
	}
	HandleScroll(){
		var percentage = ()=> {
			var h = document.documentElement, 
				b = document.body,
				st = 'scrollTop',
				sh = 'scrollHeight';
			return (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
		};  
		//Percentage scrolled function obtained from : 
		//https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript
		if(this.state.view === 0 && this.state.loading === false && percentage() > 95){
			this.LoadMore();
		}
	}
	inputHandler(event){
		var newFilters = event.target.value.split(' ');
		this.setState({
			filters: newFilters
		});
	}
	expandHandler(value){
		window.history.pushState({id: value}, "Oompa Loompa", value);
		this.setState({
			view: 1
		});
		var state=window.history.state;
		var notAvailable = true;
		var result = localStorage.getItem('SpecificData');   //Manage stored/new data
		if (result){
			result = JSON.parse(result);
			if(Math.ceil((new Date() - new Date(result.date)) / 8.64e7) < 2){ //if the data is still relevant
				var count = 0;
				while (count < result.info.length){
					if (result.info[count].id === state.id){
						this.setState({
							specific: result.info[count]
						});
						notAvailable = false;
						count = result.info.length;
					}
					count++;
				}
			}
			else {
				localStorage.removeItem('SpecificData');
			}
		}
		if (notAvailable){
			var NewPage = () => {
				fetch('https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/oompa-loompas/'+state.id)
					.then(response => response.json())
					.then(data => {
						this.setState({
							specific: data
						});
						//store new data
						var newDate = new Date();
						var newInfo = [];
						if (result){
							newInfo = result.info;
						}
						var newData = data;
						newData.id = state.id;
						newInfo.push(newData);
						var newObject = {date: newDate, info: newInfo};
						localStorage.setItem('SpecificData', JSON.stringify(newObject));
				})
				.catch(err => console.error(this.props.url, err.toString()))
			}
			NewPage();
		}	
	}
	mainView(){
		if(this.state.view != 0){
			window.history.pushState({}, "Oompa Loompa", "");
			this.setState({
				view: 0,
				specific: {},
				filters: []
			});
		}
	}
	componentDidMount(){
		this.LoadMore();
		window.addEventListener('scroll', this.HandleScroll);
	}
	componentWillUnmount(){
		window.removeEventListener('scroll', this.HandleScroll);
	}
  render() {
    return (
      <div className="App">
        <Header mainView={this.mainView} />
				{this.state.view === 0 ? <List expandHandler={this.expandHandler} filters={this.state.filters} inputHandler={this.inputHandler} list={this.state.list} /> 
				:	<Specific specific={this.state.specific}/>}
      </div>
    );
  }
}

export default App;
